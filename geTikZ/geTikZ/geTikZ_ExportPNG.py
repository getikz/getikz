# -*- coding:utf8 -*-

from __future__ import division

from gi.repository import Gtk, Poppler
import cairo
import os.path

def geTikZ_ExportPNG(dir, png_pattern, resolution, pdf_uri):
    doc = Poppler.Document.new_from_file(pdf_uri, None)
    for n in xrange(doc.get_n_pages()):
        page = doc.get_page(n)
        size_pt = page.get_size()
        size_in = [x / 72 for x in size_pt]
        size_px = [int(x * resolution) for x in size_in]
        surf = cairo.ImageSurface(cairo.FORMAT_ARGB32, size_px[0], size_px[1])
        cr = cairo.Context(surf)
        cr.set_source_rgba(0, 0, 0, 0)
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.paint()
        cr.set_operator(cairo.OPERATOR_OVER)
        cr.scale(size_px[0] / size_pt[0], size_px[1] / size_pt[1])
        doc.get_page(n).render_for_printing(cr)

        surf.write_to_png(os.path.join(dir, png_pattern % n))


class geTikZ_ExportPNGDialog(Gtk.FileChooserDialog):
    def __init__(self, parent):
        super(geTikZ_ExportPNGDialog, self).__init__("Export PNG", parent, Gtk.FileChooserAction.SELECT_FOLDER,\
            ( Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,\
              Gtk.STOCK_OK, Gtk.ResponseType.OK ))

        self.box = self.get_child()

        self.table = Gtk.Table(2, 2, homogeneous=False)
        self.pattern_label = Gtk.Label("Pattern:")
        self.pattern_label.set_justify(Gtk.Justification.RIGHT)
        self.table.attach(self.pattern_label, 0, 1, 0, 1, xoptions=Gtk.AttachOptions.FILL)
        self.pattern_entry = Gtk.Entry()
        self.pattern_entry.set_text("%04i.png")
        self.table.attach(self.pattern_entry, 1, 2, 0, 1)
        self.resolution_label = Gtk.Label("Auflösung:")
        self.resolution_label.set_justify(Gtk.Justification.RIGHT)
        self.table.attach(self.resolution_label, 0, 1, 1, 2, xoptions=Gtk.AttachOptions.FILL)
        self.resolution_spin = Gtk.SpinButton()
        self.resolution_spin.set_range(1,1600)
        self.resolution_spin.set_value(300)
        self.resolution_spin.set_increments(50, 100)
        self.resolution_spin.set_numeric(True)
        self.table.attach(self.resolution_spin, 1, 2, 1, 2)
        self.table.show_all()
        self.box.add(self.table)

        filter_png = Gtk.FileFilter()
        filter_png.set_name("PNG")
        filter_png.add_mime_type("image/png")
        self.add_filter(filter_png)

    def get_resolution(self):
        return self.resolution_spin.get_value()

    def set_resolution(self, resolution):
        self.resolution_spin.set_value(resolution)

    def get_pattern(self):
        pattern = list(os.path.splitext(self.pattern_entry.get_text()))
        if pattern[1] != '.png':
            pattern[0] += pattern[1]
            pattern[1] = '.png'
        try:
            pattern[0] % 0
        except TypeError:
            pattern[0] += "%04i"
        return pattern[0]+pattern[1]

    def set_pattern(self, pattern):
        self.pattern_entry.set_text(pattern)
