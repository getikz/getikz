#! /usr/bin/env python

from gi.repository import GObject, GLib

import sys
sys.path.append("/usr/share/rubber")
RUBBER = 'rubber'
import rubber
import fcntl
import os
import signal

class RubberCon(GObject.GObject):
    """Class to control the compilation of LaTeX to PDF-Files via rubber."""
    __gtype_name__ = 'RubberCon'
    __gsignals__ = {
#        'status_changed': (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ([object, int, object])),
        'compilation_finished': (GObject.SignalFlags.RUN_FIRST, None, ([int, object]))
    }

    def __init__(self, document):
        GObject.GObject.__init__(self)
        self.doc = document
        env = rubber.Environment()
        if env.set_source(self.doc):
            sys.stderr.write("I cannot find %s.\n" % self.doc)
            sys.exit(1)
        if env.make_source():
            sys.exit(1)
        self.child = None
        self.repeat = False
        self.output = None
        self.output_directory = None
        self.tex_path = None

    def __del__(self):
        # Ensure that we do not leave any (external) resources around
        self.kill()

    def get_dependencies(self):
        env = rubber.Environment()
        if env.set_source(self.doc):
            sys.stderr.write("I cannot find %s.\n" % self.doc)
            sys.exit(1)
        if env.make_source():
            sys.exit(1)

        env.main.parse()
        deps = []
        for dep in env.main.sources.values():
            for file in dep.leaves():
                deps.append(file)

        return deps

    def child_exit(self, pid, retcode):
        assert(self.child is not None)
        assert(self.child[0] == pid)

        output = ''.join(self.output)
        self.emit('compilation_finished', retcode, output)

        self.child = None
        self.output = None
        if self.repeat:
            self.repeat = False
            self.run(self.repeat_forced)

    def child_output(self, fd, condition):
        if not (condition & (GLib.IO_IN | GLib.IO_PRI)):
            return False

        assert(self.child is not None)

        data = os.read(fd, 2048)
        output = data.decode('utf-8', 'replace')
        output = output.encode('utf-8')
        self.output.append(output)

        if len(data) == 0:
            return False
        else:
            return True

    def run(self, force=False):
        if self.child is not None:
            self.repeat = True
            self.repeat_forced = force
            return

        self.output = []
        args = [RUBBER]
        if force:
            args.append('-f')
        args.append('--pdf')
        if self.output_directory:
            args.append('--into')
            args.append(self.output_directory)
        else:
            args.append('--inplace')
        if self.tex_path is not None:
            if isinstance(self.tex_path, (list, tuple)):
                for path in self.tex_path:
                    args.append('--texpath')
                    args.append(path)
            else:
                args.append('--texpath')
                args.append(self.tex_path)
        args.append(self.doc)

        def setup_env(*args):
            os.setpgrp()

            if self.output_directory:
                os.chdir(self.output_directory)
            else:
                os.chdir(os.path.dirname(self.doc))

        self.child = GLib.spawn_async(args, flags=GLib.SPAWN_SEARCH_PATH|GLib.SPAWN_DO_NOT_REAP_CHILD, child_setup=setup_env, standard_output=True, standard_error=True)
        GLib.child_watch_add(self.child[0], self.child_exit, priority=GLib.PRIORITY_LOW)
        GLib.io_add_watch(self.child[2], GLib.IO_IN|GLib.IO_PRI|GLib.IO_ERR|GLib.IO_HUP|GLib.IO_NVAL, self.child_output)
        GLib.io_add_watch(self.child[3], GLib.IO_IN|GLib.IO_PRI|GLib.IO_ERR|GLib.IO_HUP|GLib.IO_NVAL, self.child_output)

        # Set non-blocking IO
        fl = fcntl.fcntl(self.child[2], fcntl.F_GETFL)
        fcntl.fcntl(self.child[2], fcntl.F_SETFL, fl | os.O_NONBLOCK)
        fl = fcntl.fcntl(self.child[3], fcntl.F_GETFL)
        fcntl.fcntl(self.child[3], fcntl.F_SETFL, fl | os.O_NONBLOCK)

    def kill(self):
        if self.child is None:
            return
        try:
            os.killpg(self.child[0], signal.SIGTERM)
        except OSError:
            pass

    def clean(self):
        args = [RUBBER]
        if self.output_directory:
            args.append('--into')
            args.append(self.output_directory)
        else:
            args.append('--inplace')
        args.append('--clean')
        args.append(self.doc)
        # Just run and forget about it
        GLib.spawn_async(args, flags=GLib.SPAWN_SEARCH_PATH)

    def clean_all(self):
        args = [RUBBER]
        args.append('--pdf')
        if self.output_directory:
            args.append('--into')
            args.append(self.output_directory)
        else:
            args.append('--inplace')
        args.append('--clean')
        args.append(self.doc)
        # Just run and forget about it
        GLib.spawn_async(args, flags=GLib.SPAWN_SEARCH_PATH)


