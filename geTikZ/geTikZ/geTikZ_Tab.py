# -*- coding:utf8 -*-

from gi.repository import GLib, GObject, Gio, Gedit, Pango, EvinceView, EvinceDocument, UPowerGlib
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GtkSource

import re
from .rubbercon import RubberCon
from .geTikZ_Configure import get_settings
from .geTikZ_Gutter import geTikZ_Gutter
import tempfile
import os.path, shutil

class geTikZ_Tab(GObject.GObject):
    """Class to control the compilation of LaTeX to PDF-Files via rubber."""
    __gtype_name__ = 'geTikZ_Tab'
    __gsignals__ = {
        'update_view': (GObject.SignalFlags.RUN_FIRST, None, ([])),
        'update_panel': (GObject.SignalFlags.RUN_FIRST, None, ([]))
    }


    def __init__(self, tab):
        GObject.GObject.__init__(self)

        self.tab = tab

        self.settings = get_settings()
        self.settings.connect('changed', self.setting_changed)

        self.evmodel = None
        self.evdoc = None

        self.errors = []

        self.rubber = None
        self.rubber_output = None
        self.tmp_dir = None

        self.upower = UPowerGlib.Client()

        self.page_infos = dict()

        self._start_compilation_timeout_id = None

        self.doc = tab.get_document()
        self.saved    = self.doc.connect("loaded", self.document_loaded)
        self.saved    = self.doc.connect("saved", self.document_saved)
        self.changed  = self.doc.connect("changed", self.text_changed)
        self.mark_set = self.doc.connect("mark-set", self.marker_changed)

        self.ev_page_changed = None

        self._gutter = self.tab.get_view().get_gutter(Gtk.TextWindowType.LEFT)

        self._renderer = geTikZ_Gutter()

        self._view_style_updated_cb()
        self.tab.get_view().connect("style-updated", self._view_style_updated_cb)

        if self.settings.get_string('picture-highlighting') in ['gutter', 'both']:
            self._gutter_added = True
            self._gutter.insert(self._renderer, -40)
        else:
            self._gutter_added = False

    def clean_up(self):
        self.doc.disconnect(self.saved)
        self.doc.disconnect(self.changed)
        self.doc.disconnect(self.mark_set)

        if self.evmodel is not None:
            self.evmodel.disconnect(self.ev_page_changed)

        if self._start_compilation_timeout_id is not None:
            GLib.source_remove(self._start_compilation_timeout_id)

        if self.rubber is not None:
            self.rubber.kill()
            self.rubber = None
        if self.tmp_dir is not None:
            shutil.rmtree(self.tmp_dir)
            self.tmp_dir = None


    def ev_show(self):
        doc = self.tab.get_document()
        lang = doc.get_language()
        if lang is None or \
          not lang.get_id() == 'latex' or \
          not self.check_activatet(doc):
            return False

        return True

    def _view_style_updated_cb(self, *args):
        layout = self.tab.get_view().create_pango_layout("x")
        self._renderer.set_size(layout.get_pixel_size()[0])

        context = self.tab.get_view().get_style_context()
        self._renderer.color = context.get_background_color(Gtk.StateFlags.SELECTED)


    def setting_changed(self, settings, key):
        if key == 'picture-highlighting':
            GObject.idle_add(self.highlight_picture_source)

            visible = self.settings.get_string('picture-highlighting') in ['gutter', 'both']

            if visible != self._gutter_added:
                if visible:
                    self._gutter.insert(self._renderer, -40)
                else:
                    self._gutter.remove(self._renderer)

            self._gutter_added = visible


    def marker_changed(self, doc, iter, marker):
        if marker.get_name() == 'insert':
            if self._start_compilation_timeout_id is not None:
                self.queue_start_compilation(doc)


    def text_changed(self, doc):
        self.queue_start_compilation(doc)


    def document_saved(self, doc, errors):
        self.start_compilation(doc)


    def document_loaded(self, doc, errors):
        if self.rubber is not None:
            self.rubber.kill()
            self.rubber = None

        self.start_compilation(doc)


    def queue_start_compilation(self, doc):
        if self._start_compilation_timeout_id is not None:
            GLib.source_remove(self._start_compilation_timeout_id)

        mode = self.settings.get_string('use-background-compilation')
        if mode == 'never':
            return
        if mode == 'ac-only' and self.upower.props.on_battery:
            return

        delay = int(self.settings.get_double('compilation-delay') * 1000)
        self._start_compilation_timeout_id = GLib.timeout_add(delay, self.start_compilation, doc)


    def start_compilation(self, doc):
        if self._start_compilation_timeout_id is not None:
            GLib.source_remove(self._start_compilation_timeout_id)
        self._start_compilation_timeout_id = None

        lang = doc.get_language()
        if lang is None:
            return
        if lang.get_id() != 'latex':
            return

        if not self.check_activatet(doc):
            return

        self.errors = []
        settings = self.prepare_settings(doc)
        tikz_code, settings = self.parse_document(doc, settings)
        if self.tmp_dir is None:
            self.tmp_dir = tempfile.mkdtemp(prefix="getikz-")
        doc_path, doc_name = os.path.split(doc.get_location().get_path())
        settings['template'] = os.path.expanduser(settings['template'])
        if not os.path.isabs(settings['template']):
            settings['template'] = os.path.join(doc_path, settings['template'])
        res = self.prepare_document(self.tmp_dir, tikz_code, settings)
        if not res:
            self.emit('update_panel')
            return
        if self.rubber is None:
            self.rubber = RubberCon(os.path.join(self.tmp_dir, 'getikz_wrapper.tex'))
            self.rubber.output_directory = self.tmp_dir
            self.rubber.connect("compilation_finished", self.compilation_finished, settings)
        else:
            # Kill running compilation so we get a new compilation even if
            # previous cumpilations hung up
            self.rubber.kill()
        self.rubber.tex_path = [doc_path, ] + settings['path']
        self.rubber.run()
        self.emit('update_panel')


    def check_activatet(self, doc):
        text = doc.get_text(doc.get_start_iter(), doc.get_end_iter(), False)
        re_active = re.compile(r'^\s*(?<!\\)%[\s]*getikz[\s]*?enable[\s]*?(true|yes|on).*$', re.MULTILINE | re.IGNORECASE)
        return re_active.search(text) is not None


    def prepare_settings(self, doc):
        settings = dict()

        doc_path, doc_name = os.path.split(doc.get_location().get_path())
        doc_name, doc_ext = os.path.splitext(doc_name)
        settings['file'] = dict()
        settings['file']['path'] = doc_path
        settings['file']['name'] = doc_name
        settings['file']['extension'] = doc_ext

        # The encoding as returned by Gedit is always utf8
        # Keep the settings, so that old templates keep working.
        settings['encoding'] = 'utf8'

        settings['header'] = []

        settings['library'] = []

        settings['path'] = []

        if os.path.exists(os.path.expanduser('~/.getikz/default.tpl')):
           settings['template'] = '~/.getikz/default.tpl'
        else:
            settings['template'] = '/usr/share/getikz/default.tpl'

        return settings


    def parse_document(self, doc, settings):
        text = doc.get_text(doc.get_start_iter(), doc.get_end_iter(), False)
        re_getikz = re.compile(r'^\s*(?<!\\)%[\s]*getikz[\s]+(?P<command>[\S]*)[\s]+(?P<parameter>.*)$', re.MULTILINE | re.IGNORECASE)
        # Parse geTikZ-commands
        for match in re_getikz.finditer(text):
            cmd = match.group('command')
            cmd.lower()
            param = match.group('parameter')
            if cmd == 'enable':
                pass
            elif cmd == 'template':
                settings['template'] = param
            elif cmd == 'header':
                settings['header'].append(param)
            elif cmd == 'library':
                settings['library'].append(param)
            elif cmd == 'path':
                if not os.path.isabs(param):
                    param = os.path.join(settings['file']['path'], param)
                settings['path'].append(os.path.normpath(param))
            else:
                subtext = text[:match.start()+1].splitlines()
                zeile = len(subtext)
                spalte = len(subtext[-1])
                msg = "Unknown geTikZ-command at position %i:%i" % (zeile, spalte)
                self.errors.append(msg)
        # Strip geTikZ-commands
        text = re_getikz.sub('', text)
        return text, settings


    def prepare_document(self, tmp_dir, tikz_code, settings):
        template = None
        tpl = None
        try:
            tpl = open(settings['template'], 'r')
            template = tpl.read()
        except Exception:
            self.errors.append("Cold not load template file '%s'" % settings['template'])
        finally:
            if tpl:
                tpl.close()

        if not template:
            return False

        # Add support for picture information:
        getikz_info_header = r"""
%% support for picture information ---------------------------------------------
\usepackage[mainext=<file.extension>]{currfile}

\makeatletter
\newcounter{@getikz@picture@0}
\newcounter{@getikz@picture@level}

\newwrite\@getikz@file%
\immediate\openout\@getikz@file=\jobname.getikz.info%

\newcommand{\@getikz@ensure@counter}{
  \@ifundefined{c@@getikz@picture@\the@getikz@picture@level}{
    \@tempcntb=\the@getikz@picture@level
    \advance\@tempcntb by -1
    \newcounter{@getikz@picture@\the@getikz@picture@level}[@getikz@picture@\the\@tempcntb]
  }{
  }
}

\newcommand{\the@getikz@picture}{%
  \ifnum\value{@getikz@picture@level}>0 %
    \csname the@getikz@picture@0\endcsname.\the@getikz@picture@level.\csname the@getikz@picture@\the@getikz@picture@level\endcsname%
  \else%
    \csname the@getikz@picture@0\endcsname%
  \fi%
}

\newcommand{\@getikz@picture@started}{
  \@getikz@ensure@counter
  \stepcounter{@getikz@picture@\the@getikz@picture@level}
  \immediate\write\@getikz@file{\the@getikz@picture\space start @ \currfilepath\space\number\inputlineno}
  \stepcounter{@getikz@picture@level}
}

\newcommand{\@getikz@picture@ended}{
  \addtocounter{@getikz@picture@level}{-1}
  \pgfpointdiff{\pgfpointanchor{current bounding box}{south west}}{\pgfpointanchor{current bounding box}{north east}}
  \pgfgetlastxy{\@getkiz@picture@wd}{\@getkiz@picture@hd}
  \immediate\write\@getikz@file{\the@getikz@picture\space size\space\space (\@getkiz@picture@wd, \@getkiz@picture@hd)}
  \immediate\write\@getikz@file{\the@getikz@picture\space end\space\space\space @ \currfilepath\space\number\inputlineno}
}

\tikzset{
  /tikz/every picture/.style={
    execute at begin picture=\@getikz@picture@started,
    execute at end picture=\@getikz@picture@ended,
  },
}
\makeatother
%% -----------------------------------------------------------------------------
"""
        getikz_info_footer = r"""
%% support for picture information ---------------------------------------------
\makeatletter
\immediate\closeout\@getikz@file
\makeatother
%% -----------------------------------------------------------------------------
"""
        getikz_info_header = getikz_info_header.replace('<file.extension>', settings['file']['extension'].lstrip('.'))
        settings['header'].append(getikz_info_header)

        settings['tikz_code'] = '\\input{getikz_target}'
        settings['tikz_code'] += getikz_info_footer

        for key in ['encoding', 'header', 'library', 'tikz_code']:
            sub = settings[key]
            if type(sub) is list:
                if key == 'library':
                    sub = ','.join(sub)
                else:
                    sub = '\n'.join(sub)
            needle = "<%s>" % key
            template = template.replace(needle, sub)


        success = True
        tikz = None
        try:
            tikz = open(os.path.join(tmp_dir, "getikz_target.tex"), 'w')
            tikz.write(tikz_code)

            tmpl = open(os.path.join(tmp_dir, "getikz_wrapper.tex"), 'w')
            tmpl.write(template)
        except Exception:
            self.errors.append("Cold not create required tex files.")
            success = False
        finally:
            if tikz:
                tikz.close()
                self.tikz_src = template
        return success


    def extract_info(self, info_file):
        infof = None
        info = ''
        self.page_infos = dict()
        try:
            infof = open(info_file, 'r')
            info = infof.read()
        except Exception:
            pass
        finally:
            if infof:
                infof.close()

        info_expressions = [\
            r'^(?P<page>\d+)\s+start\s+@\s+(?P<start_path>.*?)\s+(?P<start_line>\d+)$',\
            r'^(?P<page>\d+)\s+size\s+\((?P<width>[\d.]+)pt,\s+(?P<height>[\d.]+)pt\)$',\
            r'^(?P<page>\d+)\s+end\s+@\s+(?P<end_path>.*?)\s+(?P<end_line>\d+)$']
        for expr in info_expressions:
            re_info = re.compile(expr, re.MULTILINE)
            for match in re_info.finditer(info):
                page = int(match.group('page'))
                if not self.page_infos.has_key(page):
                    self.page_infos[page] = dict()
                for k,v in match.groupdict().iteritems():
                    if k == 'page':
                        continue
                    if k in ['start_line', 'end_line']:
                        v = int(v)
                    if k in ['width', 'height']:
                        v = float(v) * 25.4 / 72.27
                    self.page_infos[page][k] = v

        GObject.idle_add(self.update_markers)
        self.emit('update_panel')


    def prepare_rubberoutput(self, rubber, output, settings):
        output = output.replace(os.path.join(self.tmp_dir, ''), '')
        output = output.replace('getikz_target.tex', settings['file']['name'] + settings['file']['extension'])
        return output

    def compilation_finished(self, rubber, retcode, output, settings):
        tex = rubber.doc
        if retcode == 0:
            self.rubber_output = None
        else:
            self.rubber_output = self.prepare_rubberoutput(rubber, output, settings)
        getikz_info = os.path.splitext(tex)[0] + '.getikz.info'
        source_pdf = os.path.splitext(tex)[0] + '.pdf'
        dest_pdf = os.path.join(settings['file']['path'], settings['file']['name'] + '.pdf')
        if os.path.exists(source_pdf):
            shutil.move(source_pdf, dest_pdf)
            pdf = Gio.file_new_for_path(dest_pdf)
            try:
                self.evdoc = EvinceDocument.Document.factory_get_document(pdf.get_uri())
            except GObject.GError, e:
                 pass
            else:
                if self.evmodel is None:
                    self.evmodel = EvinceView.DocumentModel()
                    self.ev_page_changed= self.evmodel.connect("page_changed", self.page_changed)
                    self.emit('update_view')
                self.evmodel.set_document(self.evdoc)
            GObject.idle_add(self.extract_info, getikz_info)
        self.emit('update_panel')


    def update_markers(self):
        buf = self.tab.get_view().get_buffer()

        for page, infos in self.page_infos.iteritems():
            try:
                env_start = buf.get_iter_at_line(infos['start_line'] - 1 - 1)
                env_end   = buf.get_iter_at_line(infos['end_line'] - 1)
            except KeyError:
                continue
            if not env_end.ends_line():
                env_end.forward_to_line_end()

            start_mark = buf.get_mark("tikz_environ_%i_start" % page)
            if start_mark is None:
                buf.create_mark("tikz_environ_%i_start" % page, env_start, False)
            else:
                buf.move_mark(start_mark, env_start)

            stop_mark = buf.get_mark("tikz_environ_%i_end" % page)
            if start_mark is None:
                buf.create_mark("tikz_environ_%i_end" % page, env_end, True)
            else:
                buf.move_mark(stop_mark, env_end)

        GObject.idle_add(self.highlight_picture_source)


    def page_changed(self, model, old, new):
        GObject.idle_add(self.highlight_picture_source)


    def highlight_picture_source(self):
        if not self.evmodel:
            return
        page = self.evmodel.get_page()+1

        buf = self.tab.get_view().get_buffer()

        start_mark = buf.get_mark("tikz_environ_%i_start" % page)
        if start_mark is None:
            return
        end_mark = buf.get_mark("tikz_environ_%i_end" % page)
        if end_mark is None:
            return

        start_iter = buf.get_start_iter()
        end_iter = buf.get_end_iter()
        if buf.get_tag_table().lookup('getikz:hidden'):
            buf.remove_tag_by_name('getikz:hidden', start_iter, end_iter)

        if not buf.get_tag_table().lookup('getikz:hidden'):
            scheme = buf.get_style_scheme()
            style = scheme.get_style('getikz:hidden')
            tag = buf.create_tag('getikz:hidden')
            tag.set_priority(0)
            if style is not None:
                tag.set_property('foreground', style.get_property('foreground'))
            else:
                context = self.tab.get_style_context()
                state = Gtk.StateFlags.BACKDROP
                color = context.get_color(state)
                tag.set_property('foreground_rgba', color)

        env_start = buf.get_iter_at_mark(start_mark)
        env_end = buf.get_iter_at_mark(end_mark)

        if self.settings.get_string('picture-highlighting') in ['code_highlight', 'both']:
            buf.apply_tag_by_name('getikz:hidden', start_iter, env_start)
            buf.apply_tag_by_name('getikz:hidden', env_end, end_iter)

        self._renderer.set_data(buf, page)
        if self._gutter_added:
            self._gutter.queue_draw()

