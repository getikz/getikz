# -*- coding:utf8 -*-

from gi.repository import Gtk, EvinceView
from geTikZ_ExportPNG import geTikZ_ExportPNG, geTikZ_ExportPNGDialog
import os.path, urlparse, urllib

class geTikZ_evToolbar(Gtk.Toolbar):
    def __init__(self):
        super(geTikZ_evToolbar, self).__init__()
        EvinceView.stock_icons_init()
        self.evmodel = None

        self.prev_page = Gtk.ToolButton(Gtk.STOCK_GO_UP)
        self.add(self.prev_page)
        self.next_page = Gtk.ToolButton(Gtk.STOCK_GO_DOWN)
        self.add(self.next_page)
        self.add(Gtk.SeparatorToolItem())
        self.continous = Gtk.ToggleToolButton(EvinceView.STOCK_VIEW_CONTINUOUS)
        self.add(self.continous)
        self.fit = Gtk.ToggleToolButton(EvinceView.STOCK_ZOOM_PAGE)
        self.add(self.fit)
        self.add(Gtk.SeparatorToolItem())
        self.export_png = Gtk.ToolButton(Gtk.STOCK_CONVERT)
        self.add(self.export_png)
        self.show_all()

        self.sig_export_png = None
        self.sig_prev_page = None
        self.sig_next_page = None
        self.sig_page = None
        self.sig_continous = None
        self.sig_fit = None
        self.sig_scale = None


    def export_png_cb(self, widget):
        pdf_uri = self.evmodel.get_document().get_uri()
        path = urllib.url2pathname(urlparse.urlparse(pdf_uri).path)
        dirname, filename = os.path.split(os.path.splitext(path)[0])
        pattern  = filename + "-%04i.png"

        dialog = geTikZ_ExportPNGDialog(self.get_toplevel())
        dialog.set_filename(dirname)
        dialog.set_pattern(pattern)

        response = dialog.run()
        try:
            if response == Gtk.ResponseType.OK:
                geTikZ_ExportPNG(dialog.get_filename(), dialog.get_pattern(), dialog.get_resolution(), pdf_uri)
        finally:
            dialog.destroy()


    def connect(self, model):
        assert isinstance(model, EvinceView.DocumentModel)
        self.evmodel = model

        self.sig_prev_page = self.prev_page.connect('clicked', self.prev_page_cb)
        self.sig_next_page = self.next_page.connect('clicked', self.next_page_cb)
        self.sig_page = self.evmodel.connect('notify::page', self.page_cb)

        self.sig_continous = self.continous.connect('toggled', self.continous_cb)
        self.continous.set_active(self.evmodel.get_continuous())

        self.sig_fit = self.fit.connect('toggled', self.fit_cb)
        self.sig_scale = self.evmodel.connect('notify::scale', self.scale_cb)
        mode = self.evmodel.get_sizing_mode()
        active = False
        if mode == EvinceView.SizingMode.BEST_FIT:
            active = True
        self.fit.set_active(active)

        self.sig_export_png = self.export_png.connect('clicked', self.export_png_cb)


    def disconnect(self):
        if self.evmodel is None:
            return

        if self.sig_prev_page:
            self.prev_page.disconnect(self.sig_prev_page)
            self.sig_prev_page = None
        if self.sig_next_page:
            self.next_page.disconnect(self.sig_next_page)
            self.sig_next_page = None
        if self.sig_page:
            self.evmodel.disconnect(self.sig_page)
            self.sig_page = None

        if self.sig_continous:
            self.continous.disconnect(self.sig_continous)
            self.sig_continous = None

        if self.sig_fit:
            self.fit.disconnect(self.sig_fit)
            self.sig_fit = None
        if self.sig_scale:
            self.evmodel.disconnect(self.sig_scale)
            self.sig_scale = None

        if self.sig_export_png:
            self.export_png.disconnect(self.sig_export_png)
            self.sig_export_png = None


    def prev_page_cb(self, widget):
        newpage = self.evmodel.get_page() - 1
        self.evmodel.set_page(newpage)


    def next_page_cb(self, widget):
        newpage = self.evmodel.get_page() + 1
        self.evmodel.set_page(newpage)


    def page_cb(self, model, page):
        page = model.get_page() + 1
        pages = model.get_document().get_n_pages()
        prev = page > 1
        next = page < pages
        self.prev_page.set_sensitive(prev)
        self.next_page.set_sensitive(next)
        self.export_png.set_sensitive(pages > 0)


    def continous_cb(self, widget):
        self.evmodel.set_continuous(widget.get_active())
        widget.set_active(self.evmodel.get_continuous())


    def fit_cb(self, widget):
        if widget.get_active():
            new_mode = EvinceView.SizingMode.BEST_FIT
        else:
            new_mode = EvinceView.SizingMode.FREE
        self.evmodel.set_sizing_mode(new_mode)
        mode = self.evmodel.get_sizing_mode()
        active = False
        if mode == EvinceView.SizingMode.BEST_FIT:
            active = True
        widget.set_active(active)


    def scale_cb(self, model, scale):
        mode = self.evmodel.get_sizing_mode()
        active = False
        if mode == EvinceView.SizingMode.BEST_FIT:
            active = True
        self.fit.set_active(active)

