# -*- coding:utf8 -*-

from gi.repository import GObject, Gedit, Gtk, GtkSource, EvinceView, EvinceDocument, PeasGtk
from .geTikZ_Tab import geTikZ_Tab
from .geTikZ_Configure import geTikZ_Configure, init_settings
from .geTikZ_evToolbar import geTikZ_evToolbar

init_settings(__path__[0])


MENU_XML = """<ui>
<menubar name="MenuBar">
    <menu name="ToolsMenu" action="Tools">
      <placeholder name="ToolsOps_4">
        <menuitem name="geTikZEnableAction" action="geTikZEnableAction"/>
      </placeholder>
    </menu>
</menubar>
</ui>"""

GETIKZ_HEADER = """% getikz enable true
%! Available geTikZ-commands:
%! % getikz library <required tikz/pgf library>
%! % getikz header <header e.g. \usepackage commands>
%! % getikz path <additional location to search for files>
%! For a description of these and other commands have a look at
%! https://gitorious.org/getikz/pages/Commands

"""

class geTiKZ(GObject.Object, Gedit.WindowActivatable, PeasGtk.Configurable):
    __gtype_name__ = "geTiKZ"

    window = GObject.property(type=Gedit.Window)


    def __init__(self):
        GObject.Object.__init__(self)

        EvinceDocument.init()

        self._window_signals = []

        self.evview = None
        self.evtoolbar = None


    def tab_removed_cb(self, win, tab):
        if tab.__dict__.has_key('geTikZ'):
            tab.geTikZ.clean_up()
        # Close EvDocument
        if self.evview:
            evmodel = EvinceView.DocumentModel()
            self.evview.set_model(evmodel)


    def tab_changed_cb(self, win, tab=None):
        if tab is None:
            tab = self.window.get_active_tab()

        # another check
        if tab is None:
            return

        if not tab.__dict__.has_key('geTikZ'):
            tab.geTikZ = geTikZ_Tab(tab)
            tab.geTikZ.connect('update_view', self.update_view)
            tab.geTikZ.connect('update_panel', self.update_panel)

        GObject.idle_add(self.update_view, tab.geTikZ)
        GObject.idle_add(self.update_panel, tab.geTikZ)

        self.update_view(tab)


    def add_menu(self):
        manager = self.window.get_ui_manager()
        self._actions = Gtk.ActionGroup("geTikZActions")
        self._actions.add_actions([
            ('geTikZEnableAction', Gtk.STOCK_INFO, "Enable geTikZ",
                None, "Enable geTikz for the current document.",
                self.on_geTikZEnableAction_activate),
        ])
        manager.insert_action_group(self._actions)
        self._ui_merge_id = manager.add_ui_from_string(MENU_XML)
        manager.ensure_update()


    def remove_menu(self):
        manager = self.window.get_ui_manager()
        manager.remove_ui(self._ui_merge_id)
        manager.remove_action_group(self._actions)
        manager.ensure_update()


    def on_geTikZEnableAction_activate(self, action, data=None):
        tab = self.window.get_active_tab()
        if tab:
            doc = tab.get_document()
            if not tab.geTikZ.check_activatet(doc):
                doc.insert(doc.get_start_iter(), GETIKZ_HEADER)
            manager = GtkSource.LanguageManager()
            lang = manager.get_language('latex')
            tab.get_document().set_language(lang)
        self.update_view(tab)


    def ensure_widget(self, tab):
        def center_paned(paned):
            width = paned.get_allocation().width
            paned.set_position(width / 2)

            return False

        if self.evview is not None:
            return

        self.notebook = tab.get_parent()
        self.nb_parent = self.notebook.get_parent()
        width = self.nb_parent.get_allocation().width

        self.paned = Gtk.Paned()

        self.nb_parent.remove(self.notebook)
        self.nb_parent.add(self.paned)
        self.paned.add(self.notebook)

        self.paned.show()

        self.evview = EvinceView.View()
        self.evview.show()

        self.ev_vbox = Gtk.VBox(homogeneous=False)
        self.ev_vbox.show()
        self.paned.add(self.ev_vbox)

        self.evtoolbar = geTikZ_evToolbar()
        self.evtoolbar.show()
        self.ev_vbox.pack_start(self.evtoolbar, False, False, 0)

        scrolled_win = Gtk.ScrolledWindow()
        scrolled_win.show()
        scrolled_win.add(self.evview)
        scrolled_win.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

        self.ev_vbox.add(scrolled_win)

        GObject.idle_add(center_paned, self.paned)


    def remove_widget(self):
        if self.evview is None:
            return

        # Set a dummy model, otherwise crashes *will* happen
        # See https://bugzilla.gnome.org/show_bug.cgi?id=680023
        self.evview.set_model(EvinceView.DocumentModel())

        self.paned.remove(self.notebook)
        self.nb_parent.remove(self.paned)
        self.nb_parent.add(self.notebook)

        self.evview = None
        self.paned = None


    def add_panel(self):
        icon = Gtk.Image.new_from_stock(Gtk.STOCK_PAGE_SETUP, Gtk.IconSize.MENU)
        self._treestore = Gtk.TreeStore(str)
        self._treeview = Gtk.TreeView(self._treestore)
        self._tvcolumn = Gtk.TreeViewColumn('')
        self._treeview.append_column(self._tvcolumn)
        self._cellrendr = Gtk.CellRendererText()
        self._tvcolumn.pack_start(self._cellrendr, True)
        self._tvcolumn.add_attribute(self._cellrendr, 'text', 0)
        self._treeview.set_search_column(0)
        self._treeview.set_headers_visible(False)
        self._panel_widget = Gtk.ScrolledWindow()
        self._panel_widget.add(self._treeview)
        self._panel_widget.show_all()
        panel = self.window.get_bottom_panel()
        panel.add_item(self._panel_widget, "geTikZ", "geTikZ", icon)
        panel.activate_item(self._panel_widget)


    def remove_panel(self):
        panel = self.window.get_bottom_panel()
        panel.remove_item(self._panel_widget)


    def do_create_configure_widget(self):
        return geTikZ_Configure()


    def do_activate(self):
        self._window_signals.append(self.window.connect("active-tab-state-changed", self.tab_changed_cb))
        self._window_signals.append(self.window.connect("active-tab-changed", self.tab_changed_cb))
        self._window_signals.append(self.window.connect("tab-removed", self.tab_removed_cb))

        tab = self.window.get_active_tab()
        if tab is not None:
            self.tab_changed_cb(self.window, tab)

        self.add_panel()
        self.add_menu()
        pass


    def do_deactivate(self):
        for signal in self._window_signals:
            self.window.disconnect(signal)
        self._window_signals = []
        self.remove_widget()
        self.remove_panel()
        self.remove_menu()
        pass


    def do_update_state(self):
        pass


    def update_view(self, tab):
        if type(tab) is geTikZ_Tab:
            tab = tab.tab
        if tab is not self.window.get_active_tab():
            return

        if self.evtoolbar is not None:
            self.evtoolbar.disconnect()

        if tab.geTikZ.ev_show():
            self.ensure_widget(tab)
            if tab.geTikZ.evmodel is not None:
                self.evview.set_model(tab.geTikZ.evmodel)
                self.evtoolbar.connect(tab.geTikZ.evmodel)
            else:
                evmodel = EvinceView.DocumentModel()
                self.evview.set_model(evmodel)
        else:
            self.remove_widget()


    def update_panel(self, tab):
        if type(tab) is not geTikZ_Tab:
            return
        if tab.tab is not self.window.get_active_tab():
            return

        if not tab.ev_show():
            return

        self._treestore.clear()
        # Fehler anzeigen
        if tab.errors != []:
            err = self._treestore.append(None, ['geTikZ-Meldungen', ])
            for item in tab.errors:
                self._treestore.append(err, [item, ])
        else:
            err = None
        # Meldungen von Rubber ausgeben
        if tab.rubber_output:
            tex_errors = self._treestore.append(None, ['LaTeX-Fehler', ])
            for line in tab.rubber_output.splitlines():
                self._treestore.append(tex_errors, [line, ])
        else:
            tex_errors = None
        # Seitengrößen ausgeben
        sizes = self._treestore.append(None, ['Seitengrößen', ])
        for page in sorted(tab.page_infos.keys()):
            try:
                text = "Seite %i: %.01fmm x %.01fmm" % (page, tab.page_infos[page]['width'], tab.page_infos[page]['height'])
                self._treestore.append(sizes, [text, ])
            except KeyError:
                continue
        # Elemente expandieren
        model = self._treeview.get_model()
        if err:
            self._treeview.expand_row(model.get_path(err), True)
        if tex_errors:
            self._treeview.expand_row(model.get_path(tex_errors), True)
        self._treeview.expand_row(model.get_path(sizes), True)

