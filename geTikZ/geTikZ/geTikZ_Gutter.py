# -*- coding: utf-8 -*-

#  Copyright (C) 2013 - Ignacio Casal Quinteiro
#  Copyright (C) 2013 - Benjamin Berg <benjamin@sipsolutions.net>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330,
#  Boston, MA 02111-1307, USA.

from gi.repository import Gdk, Gtk, GtkSource

class geTikZ_Gutter(GtkSource.GutterRenderer):

    def __init__(self):
        GtkSource.GutterRenderer.__init__(self)
        self.set_size(4)
        self.color = Gdk.Color(1, 0, 0)

        self.buffer = None
        self.start_mark = None
        self.stop_mark = None

        self.hilight = False

    def do_draw(self, cr, bg_area, cell_area, start, end, state):
        GtkSource.GutterRenderer.do_draw(self, cr, bg_area, cell_area, start, end, state)

        if self.hilight:
            cr.set_source_rgba(self.color.red, self.color.green, self.color.blue, self.color.alpha)
            cr.rectangle(cell_area.x, cell_area.y, cell_area.width, cell_area.height)
            cr.fill()

    def do_query_data(self, start, end, state):
        self.hilight = False

        if self.buffer is None:
            return

        start_iter = self.buffer.get_iter_at_mark(self.start_mark)
        end_iter = self.buffer.get_iter_at_mark(self.end_mark)

        start_cmp = end.compare(start_iter)
        end_cmp = start.compare(end_iter)

        if start_cmp >= 0 and end_cmp <= 0:
            self.hilight = True

    def set_data(self, buf, page):
        self.buffer = buf
        self.page = page
        self.start_mark = buf.get_mark("tikz_environ_%i_start" % page)
        self.end_mark = buf.get_mark("tikz_environ_%i_end" % page)


# ex:ts=4:et:
