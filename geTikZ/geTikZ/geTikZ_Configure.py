# -*- coding:utf8 -*-

from gi.repository import Gio, Gtk


schema_id = "org.gnome.gedit.plugins.getikz"

def init_settings(module_path):
    global getikz_settings_path

    getikz_settings_path = module_path

def get_settings():
    global getikz_settings_path

    try:
        dummy_settings = Gio.Settings("org.gnome.gedit")
        default_source = Gio.SettingsSchemaSource.get_default()

        source = Gio.SettingsSchemaSource.new_from_directory(getikz_settings_path, default_source, False)
        schema = source.lookup(schema_id, True)

        settings = Gio.Settings.new_full(schema, None, None)
    except: # This is likely a GError, because there is no compiled schema
        # Try to look it up, maybe it is installed globally
        settings = Gio.Settings(schema_id) 

    return settings
    

class geTikZ_Configure(Gtk.Grid):
    def __init__(self):
        Gtk.Grid.__init__(self)

        self.settings = get_settings()

        self.background_compilation_label = Gtk.Label("Background compilation")
        self.background_compilation_combobox = Gtk.ComboBoxText()
        compilation_modes = self.settings.get_range('use-background-compilation').unpack()[1]
        for mode in compilation_modes:
            self.background_compilation_combobox.append(mode, mode)
        self.background_compilation_combobox.set_active_id(self.settings.get_string('use-background-compilation'))
        self.settings.connect('changed::use-background-compilation', self.enum_option_changed, self.background_compilation_combobox)
        self.background_compilation_combobox.connect('changed', self.combobox_changed, self.settings, 'use-background-compilation')
        self.attach(self.background_compilation_label, 0, 0, 1, 1)
        self.attach_next_to(self.background_compilation_combobox, self.background_compilation_label, Gtk.PositionType.RIGHT, 1, 1)

        self.compilation_delay_label = Gtk.Label("Compilation delay")
        self.compilation_delay_spinbutton = Gtk.SpinButton()
        self.compilation_delay_spinbutton.set_range(0, 1000)
        self.compilation_delay_spinbutton.set_digits(1)
        self.compilation_delay_spinbutton.set_increments(0.1, 1)
        self.compilation_delay_spinbutton.set_value(self.settings.get_double('compilation-delay'))
        self.settings.connect('changed::compilation-delay', self.double_option_changed, self.compilation_delay_spinbutton)
        self.compilation_delay_spinbutton.connect('changed', self.spinbutton_changed, self.settings, 'compilation-delay')
        self.attach_next_to(self.compilation_delay_label, self.background_compilation_label, Gtk.PositionType.BOTTOM, 1, 1)
        self.attach_next_to(self.compilation_delay_spinbutton, self.compilation_delay_label, Gtk.PositionType.RIGHT, 1, 1)

        highlighting_methods = self.settings.get_range('picture-highlighting').unpack()[1]

        self.highlighting_label = Gtk.Label("Highlight method")
        self.highlighting_combobox = Gtk.ComboBoxText()
        for methode in highlighting_methods:
            self.highlighting_combobox.append(methode, methode)
        self.highlighting_combobox.set_active_id(self.settings.get_string('picture-highlighting'))
        self.settings.connect('changed::picture-highlighting', self.enum_option_changed, self.highlighting_combobox)
        self.highlighting_combobox.connect('changed', self.combobox_changed, self.settings, 'picture-highlighting')
        self.attach_next_to(self.highlighting_label, self.compilation_delay_label, Gtk.PositionType.BOTTOM, 1, 1)
        self.attach_next_to(self.highlighting_combobox, self.highlighting_label, Gtk.PositionType.RIGHT, 1, 1)

        self.show_all()

    def bool_option_changed(self, setting, key, checkbutton):
        checkbutton.set_active(setting.get_boolean(key))

    def checkbutton_toggled(self, checkbutton, setting, key):
        setting.set_boolean(key, checkbutton.get_active())

    def double_option_changed(self, setting, key, spinbutton):
        spinbutton.set_value(setting.get_double(key))

    def spinbutton_changed(self, spinbutton, setting, key):
        setting.set_double(key, spinbutton.get_value())

    def enum_option_changed(self, setting, key, combobox):
        combobox.set_active_id(setting.get_string(key))

    def combobox_changed(self, combobox, setting, key):
        setting.set_string(key, combobox.get_active_id())

